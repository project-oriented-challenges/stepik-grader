IU Stepik Grader checker
====

This documents describes requirements for the docker image that could be used as the checker by the IU Stepik Grader.

## Docker image requirements

The Docker image must be published on [Docker Hub](https://hub.docker.com/) in a public repository. It is recommended to not use a trivial name for the repository in order to avoid revealing of test process to potential students.

The Docker image is pulled automatically by the grader runner. The runner gets the image name from the `grader_payload` field of JSON provided by the Stepik. See the description of the `grader_payload` field below.

The checker provided by the image must care about time which requires to check the solution and stop the checking process accrodingly with the corresponding information message for the student that submit the solution.

### Input and output

The Docker container with the checker will be started with the mounted directory `/data`.

  * A student input from the text field is stored in `/data/input/student_response.txt`
  * Files submitted by the student are stored in `/data/input/files/` by corresponding names (provided in the configuration on Stepik)

The checker must store results of the solution checking in `/data/output/result.json`.

If after termination of the checker container there will be no `result.json` file (uncaught exception in the student solution or in the checker) the student will get 0 with message "Error occurred while running you program".

#### result.json format 

```json
{
  "score": 0.53,
  "msg": "7 of 13 tests passed!"
}
```

where
  * `score` is a value in the range from `0` to `1` depending on the approach used to check the student solution
  * `msg` is a string (`\n` supported) that will be passed to the students as feedback from the checker.



### Container termination

In case of timeout or runner termination process the Docker container with the checker will receive SIGTERM and will have 5 seconds before receiving SIGKILL to gracefully terminate.

No `result.json` in this case is required

## Step configuration

The process of creation a step that uses an external grader described in [the official Stepik documentation](https://stepik.org/lesson/50675).

In order to use external grader for such kind of steps it is necessary to know a queue name the grader is listening to.

IU Stepik grader relies on data of the `grader_payload` field that. The format of the field is the following:
```json
"grader_payload": {
    "key": "some.unique.id",
    "checker_image": "your/dockerimage",
    "timeout": 10
}
```

where
  * `key` - an unique id of the task (step). Since the grader can be used to check different steps in one (or several) courses) this sub-filed is used to correlate results of the students submissions sent to the grader with the corresponding checker.
  * `checker_image` - the name of the Docker image that will be pulled by the grader from Docker Hub.
  * `timeout` - maximum time that will be provided to the checker to handle one solution submit. This timeout is only to prevent thhe checker from infinite running.

## Examples
  * [Simple checker for a Python programing assignment](simple-checker)
