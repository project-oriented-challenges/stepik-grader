IU Stepik Grader
====

This repository is used to keep the work done to implement an external grader
for [Stepik.org](https://stepik.org).

## Install and run

These steps describe how to install the grader on an Ubuntu/Linux system. It is assumed that a user performing these steps has an ability to create directory in `/opt` and manage initialization scripts and Docker.

1. Create a directory in `/opt`
   ```shell
   sudo mkdir /opt/stepik-grader
   sudo chown $USER /opt/stepik-grader
   ```
2. Clone the repo
   ```shell
   cd /opt
   git clone https://gitlab.com/project-oriented-challenges/stepik-grader.git
   ```
3. Copy the init script and register it
   ```shell
   cd stepik-grader
   sudo cp stepikgrader /etc/init.d/
   sudo update-rc.d stepikgrader defaults
   ```
4. Prepare the grader configuration:
   ```shell
   cp .env.example .env
   ```
5. Modify the content of the configuration file with data received from Stepik.org
   ```
   XQUEUE_NAME=some_queue
   XQUEUE_USERNAME=login
   XQUEUE_PASSWORD=password
   ```
6. Run the grader
   ```shell
   sudo systemctl start stepikgrader
   ```

After these actions the grader will be run automatically after the system reboot.

*Note:* If the grader is going to be run from another directory rather than `/opt/stepik-grader`, the working path must be changed in the `stepikgrader` init script.

### Logging

By default as it is configured in the `docker-compose.yml` file logs are sent to the `syslog` facility and available via `/var/log/syslog`.

#### Dedicated files for docker logs

It is possible to configure sending grader logs in dedicated files by performing the following steps (applicable for `rsyslog` only):

1. Copy the configuration file to the `/etc/rsyslog.d/` directory:
   ```shell
   sudo cp 30-common-docker.conf 31-grader-docker.conf /etc/rsyslog.d/
   ```
2. Restart the `rsyslog` service
   ```shell
   sudo service rsyslog restart
   ```

After that logs will be available in `/var/log/docker/`. Separate subdirectories will be used for separate grader components.

#### Log rotation

Log rotation is useful to manage disk space consumption by logs - as soon as the file with logs exceeds some size threshold it will be archived. Limit on number of archives will remove old archives as so the disk space will be freed. The log rotation can be configured by following command:

Copy the configuration file to the `/etc/logrotate.d/` directory:
```shell
sudo cp docker-logs-rotation /etc/logrotate.d/
```

Since the log rotation facility is run on scheduled basis by `cron` there is no need to restart any service. 

#### Forward logs to a remote server

It is useful to send the grader logs to a remote syslog server to monitor the system status there without necessity to login to a server with the grader by SSH. For example, online services like [Solarwinds Papertrail](https://papertrailapp.com/) can be used to gather logs and investigate issues by powerful mechanism of filters and search requests.

1. Modify `32-docker-remote-logging.conf` and `33-grader-remote-logging.conf` to reflect the remote server connectivity details:
   ```
   protocol="<proto>"
   target="<target>"
   port="<port>"
   ```
2. Copy the configuration files to the `/etc/rsyslog.d/` directory:
   ```shell
   sudo cp 32-docker-remote-logging.conf 33-grader-remote-logging.conf /etc/rsyslog.d/
   ```
3. Restart the `rsyslog` service
   ```shell
   sudo service rsyslog restart
   ```

### Useful commands

* To stop the grader
  ```shell
  systemctl stop stepikgrader
  ``` 

* To re-build checker images
  ```shell
  service stepikgrader rebuild
  ``` 

* To get status of containers running (must be run in `/opt/stepik-grader` directory)
  ```shell
  docker-compose ps
  ```

## Checker documentation

Available in [checker/README.md](checker/README.md)

## References
  * [Slides about pros and cons of the external grading](https://stepik.org/media/attachments/lesson/58101/22_1300_sviderski.pdf) from Stepikon-2017
  * [xqueue-watcher](https://github.com/edx/xqueue-watcher) - a reference
    implementation of an external grader that interacts XQueue used by Stepik.org
