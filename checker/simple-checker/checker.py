'''
Checker for program that sums 2 number
'''

import logging
import subprocess
import random
from shutil import copy
import json
from time import sleep

STUDENT_INPUT_FILE_PATH = '/data/input/input.txt'
STUDENT_FILES_PATH = '/data/input/files'
OUTPUT_FOLDER_PATH = '/data/output'


def run_solution(program_name, pass_input=''):
    # copies solution program in the directory where checker located
    copy(f'{STUDENT_FILES_PATH}/{program_name}', f'{program_name}')
    return subprocess.check_output(['python', f'./{program_name}'],
                                   input=f"{pass_input}".encode()).decode().strip()


def run_text_solution(program_name, pass_input=''):
    # copies student solution program from text filed in the directory where checker located
    copy(f'{STUDENT_INPUT_FILE_PATH}', f'{program_name}')

    return subprocess.check_output(['python', f'{program_name}'],
                                   input=f"{pass_input}".encode()).decode().strip()


LOG_FORMAT = ('%(levelname) -10s %(asctime)s %(name) -30s %(funcName) '
              '-35s %(lineno) -5d: %(message)s')
LOGGER = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO, format=LOG_FORMAT)

LOGGER.info(subprocess.check_output(['ls']).decode())

a, b = random.randint(0, 100000), random.randint(0, 100000)

correct_output = str(a + b)

with open('input.txt', 'w') as file:
    file.write(f'{a} {b}')

sleep(30)

output = run_solution('sum.py')

score = int(output == correct_output)
LOGGER.info(score)
result = {
    'score': score,
    'msg': ["Try again.", f"Good job."][score]
}

# print(output, correct_output)
# print(output == correct_output)
with open(f'{OUTPUT_FOLDER_PATH}/result.json', 'w') as result_file:
    json.dump(result, result_file)

LOGGER.info(subprocess.check_output('ls').decode())
