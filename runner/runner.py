from shutil import rmtree

import pika
import logging
import json
import os
import sys
import traceback
import docker
import redis
import urllib.request
from hashlib import sha256
import functools
import threading
import signal
from time import sleep, time

LOG_FORMAT = '%(levelname)s %(name)s:%(lineno)d:%(funcName)s: %(message)s'
LOGGER = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO, format=LOG_FORMAT)

kill_now = False


# used to log errors from thread
class StreamToLogger:

    def __init__(self, logger, log_level=logging.INFO):
        self.logger = logger
        self.log_level = log_level
        self.linebuf = ''

    def write(self, buf):
        for line in buf.rstrip().splitlines():
            self.logger.log(self.log_level, line.rstrip())

    def flush(self):
        pass


def exit_gracefully(signum, frame):
    LOGGER.info(f"Stopping by {signum} signal number")
    LOGGER.info(frame)
    global kill_now
    kill_now = True


def run_checker_image(docker_client, image_link, timeout):
    timeout = int(timeout * 60)

    LOGGER.info(f'Starting checker image {image_link} for {timeout} seconds')

    # TODO: use syslog driver to expose the container's logs
    container = docker_client.containers.run(image=f'{image_link}',
                                             volumes={os.environ["HOSTNAME"]: {'bind': '/data', 'mode': 'rw'}},
                                             mem_limit="1g", kernel_memory="512m", detach=True)

    short_id = container.id[:8]
    LOGGER.info(f'Checker {image_link} is run as {short_id}')

    interrupted = False
    counter = 0
    start = time()
    while container.status in ["created", "running"]:
        if counter == 0:
            LOGGER.info(f'Checker {short_id}: {container.status}, {int(time() - start)} of {timeout} seconds')
        if counter == 119:
            counter = 0
        else:
            counter += 1
        container.reload()
        sleep(0.5)
        if kill_now:
            container.stop(timeout=5)
            interrupted = True
            break

        if time() - start > timeout:
            LOGGER.error(f'Timeout in {timeout} seconds exceeded for checker {short_id}')
            container.stop()
            interrupted = True
            break

    if interrupted:
        LOGGER.warning(f'Checker {short_id} interrupted')
    else:
        LOGGER.info(f'Checker {short_id} finished')

    logs = container.logs().decode()
    LOGGER.info(f'Checker {short_id} logs:\n{logs}\n******** end of logs ********')

    container.remove()

    return logs


def hash_submission(task_key, input_field, files, volume_path):
    body_content = task_key.encode()
    body_content += input_field.encode()
    for path in files:
        with open(f"{volume_path}/input/files/{path}", 'rb') as file:
            body_content += file.read()
    return sha256(body_content).hexdigest()


def ack_message(ch, delivery_tag):
    if ch.is_open:
        ch.basic_ack(delivery_tag)
    else:
        LOGGER.error("Cannot send acknowledge, channel is closed")


def publish_result(ch, properties, result):
    if ch.is_open:
        ch.basic_publish(
            exchange='', routing_key=properties.reply_to, body=json.dumps(result),
            properties=pika.BasicProperties(
                correlation_id=properties.correlation_id,
                delivery_mode=2
            )
        )
    else:
        LOGGER.error("Cannot send result back, channel is closed")


def on_message(ch, method, properties, body, conn):
    t = threading.Thread(target=process_submission_and_report_errors,
                         args=(ch, method, properties, body, conn))
    t.start()


def process_submission_and_report_errors(ch, method, properties, body, conn):
    try:
        process_submission(ch, method, properties, body, conn)
    except:
        LOGGER.error(sys.exc_info())
        # return submission to rabbitmq because we are not restarting runner
        conn.add_callback_threadsafe(functools.partial(ch.basic_reject, delivery_tag=method.delivery_tag))


def unmount_and_remove_submission_dir(_volume_dir, _volume=None):
    if _volume is not None:
        _volume.remove()
    rmtree(_volume_dir, ignore_errors=True)


def process_submission(ch, method, properties, body, conn):
    volume_path_prefix = f'/volumes/{os.environ["HOSTNAME"]}'

    LOGGER.info(f"Received task: {body.decode()}")

    # extract file links and text field from submission
    body = json.loads(body.decode())

    if not {'checker_image', 'key'}.issubset(set(body['xqueue_body']['grader_payload'])):
        LOGGER.error("Incorrect step configuration")
        return_result(ch, properties,
                      {"msg": "Please contact to the task author",
                       "score": 0,
                       "checker_logs": "incorrect step configuration"
                       }, method, conn)
        return

    file_links = body['xqueue_files']
    student_response = body['xqueue_body']['student_response']
    task_key = body['xqueue_body']['grader_payload']['key']
    checker_image = body['xqueue_body']['grader_payload']['checker_image']
    checker_timeout = body['xqueue_body']['grader_payload'].get('timeout',
                                                                os.environ["STANDARD_TIMEOUT"])
    try:
        float(checker_timeout)
    except:
        LOGGER.warning("Incorrect timeout field provided. Default is used.")
        checker_timeout = 5

    submission_id = json.loads(properties.correlation_id)['submission_id']

    # if exception occurred in checker while running solution or
    # checker returns no result, student got 0
    try:
        # run container with checker
        link = checker_image
        timeout = checker_timeout

        api_client = docker.APIClient()
        client = docker.DockerClient()
        LOGGER.info("Connected to docker subsystem")

        redis_connection = redis.Redis(host='redis', port=6379)
        LOGGER.info("Connected to Redis DB")

        last_image_hash_bytes = redis_connection.get(f'image_{link}')
        if last_image_hash_bytes is None:
            last_image_hash_bytes = b''
        last_image_hash = last_image_hash_bytes.decode()
        if len(last_image_hash) > 0:
            LOGGER.info(f'Previously stored version of the docker image {last_image_hash[7:15]}')

        if kill_now:
            redis_connection.close()
            LOGGER.error("Exiting before pulling checker image")
            return

        LOGGER.info(f'Pulling image {link}')
        client.images.pull(f'{link}')
        new_image_id = api_client.inspect_image(f'{link}')['Id']
        LOGGER.info(f'Pulled {link} with id: {new_image_id[7:15]}')

        # checker image was updated and we need to remove all previous submissions
        # submission that is processing now will use cache for previous checker
        if last_image_hash != new_image_id:
            logged = False
            while redis_connection.scard(task_key) != 0:
                if not logged:
                    LOGGER.info("Removing previous solutions for this task")
                    logged = True
                old_submission_hash = redis_connection.spop(task_key)

                # remove only graded solution that are not in process now
                result = redis_connection.get(old_submission_hash).decode()
                if "score" in json.loads(result if result else "{}"):
                    redis_connection.delete(old_submission_hash)
                    LOGGER.info(f'Removed {old_submission_hash.decode()}')

            redis_connection.set(f'image_{link}', new_image_id)
            LOGGER.info("Update version of docker image in DB")

        # create mount point for docker volume
        volume_path = f'{volume_path_prefix}/_data'
        if not os.path.exists(volume_path):
            os.makedirs(volume_path, exist_ok=True)

        # create shared with container folders
        folders = ['input', 'output', 'input/files']
        for folder_name in folders:
            if not os.path.exists(f'{volume_path}/{folder_name}'):
                os.makedirs(f'{volume_path}/{folder_name}', exist_ok=True)

        # put student submission in input and input/files folders
        with open(f'{volume_path}/input/student_response.txt', 'w') as file_object:
            LOGGER.info("Writing student response")
            file_object.write(student_response)
        for filename in file_links:
            LOGGER.info(f'Downloading {file_links[filename]}')
            urllib.request.urlretrieve(f'{file_links[filename]}', f'{volume_path}/input/files/{filename}')

        submission_hash = hash_submission(task_key,
                                          student_response,
                                          file_links.keys(),
                                          volume_path)
        LOGGER.info(f'Submission hash: {submission_hash}')

        # check if submission was previously graded in redis
        result = redis_connection.get(submission_hash)
        if result is not None:
            result = json.loads(result.decode())
            LOGGER.info(f'Found {submission_hash} with result: {result}')

        # if found record, return result or
        # report that submission is already in process
        if type(result) == dict:
            if "score" in result:
                LOGGER.info(f"Using cached result {result}")
                result['checker_logs'] = "using cached solution"
                return_result(ch, properties, result, method, conn)
                unmount_and_remove_submission_dir(volume_path_prefix)
                return
            elif result['submission_id'] == submission_id:
                LOGGER.warning(f"Check of the submission is still in process: {result}")
                ack = functools.partial(ack_message, ch, method.delivery_tag)
                conn.add_callback_threadsafe(ack)
                unmount_and_remove_submission_dir(volume_path_prefix)
                return

        if kill_now:
            LOGGER.error("Exiting before starting checker image")
            redis_connection.close()
            unmount_and_remove_submission_dir(volume_path_prefix)
            return

        # mark this submission hash as in progress to prevent from putting in queue the same.
        redis_connection.set(submission_hash, properties.correlation_id)

        # set key expiration to let other runners check this submission in case of failure of this runner
        redis_connection.expire(submission_hash, time=timeout + 60)

        volume = docker.DockerClient().volumes.create(name=os.environ["HOSTNAME"])

        logs = run_checker_image(docker_client=client, image_link=link, timeout=timeout)

        # received SIGTERM
        if kill_now:
            LOGGER.error("Exiting while checking solution, marking submission as unchecked")
            redis_connection.delete(submission_hash)
            redis_connection.close()
            unmount_and_remove_submission_dir(volume_path_prefix, volume)
            return

        result_file_path = f'{volume_path}/output/result.json'

        if os.path.exists(result_file_path):
            # check result after checker finished
            with open(result_file_path) as result_file:
                result = json.load(result_file)

            volume.remove()

            LOGGER.info(f'Result received from checker: {result}')
            # cache checker results and remember where we put submission
            # to remove if checker was updated
            # and to cancel expiration of the key
            redis_connection.set(submission_hash, json.dumps({"score": result["score"], "msg": result["msg"]}))
            redis_connection.sadd(task_key, submission_hash)
            LOGGER.info("Submission result cached")
        else:
            LOGGER.error(f'result.json is not found')
            # don't cache results caused by failed check
            result = {
                'score': 0,
                'msg': 'Checking of solution was interrupted, most probably due to timeout expiration'
            }

            volume.remove()

    # if error occurred send 0 score for submission
    except Exception as e:
        LOGGER.error(traceback.format_tb(sys.exc_info()[-1]))
        logs = str(e)
        result = {
            'score': 0,
            'msg': 'Error occurred while running your solution'
        }

    result['checker_logs'] = logs

    # ignore errors if directory does not exists
    unmount_and_remove_submission_dir(volume_path_prefix)

    # send result to the xqueue_watcher
    return_result(ch, properties, result, method, conn)
    LOGGER.info("Result has been reported back")


def return_result(ch, properties, result, method, conn):
    # Using thread safe method to communicate with queue
    publish = functools.partial(publish_result, ch, properties, result)
    conn.add_callback_threadsafe(publish)

    ack = functools.partial(ack_message, ch, method.delivery_tag)
    conn.add_callback_threadsafe(ack)


def main():
    connection = pika.BlockingConnection(pika.ConnectionParameters(host='rabbitmq'))
    channel = connection.channel()

    channel.queue_declare('tasks', durable=True)

    LOGGER.info(' [*] Waiting for messages')

    # prefetch_count must be 1 as docker use shared volume to send data between host and container
    channel.basic_qos(prefetch_count=1)

    # partial(g, x=1)(2) == g(2, x=1)
    on_message_callback = functools.partial(on_message, conn=connection)
    channel.basic_consume(
        queue='tasks', on_message_callback=on_message_callback)

    signal.signal(signal.SIGINT, exit_gracefully)
    signal.signal(signal.SIGTERM, exit_gracefully)

    while not kill_now:
        try:
            connection.process_data_events()  # if connection were closed by rabbitmq
        except:
            LOGGER.error(sys.exc_info())
            LOGGER.error('connection was closed while processing update')
        sleep(0.5)

    connection.close()
    LOGGER.info("exited")


if __name__ == '__main__':
    stderr_logger = logging.getLogger('STDERR')
    sys.stderr = StreamToLogger(stderr_logger, logging.ERROR)
    main()
