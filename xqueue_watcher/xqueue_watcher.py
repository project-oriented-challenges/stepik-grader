import pika
import json
import requests
from time import sleep
import logging
import os
import redis
import signal
import functools
import threading

LOG_FORMAT = '%(levelname)s %(name)s:%(lineno)d:%(funcName)s: %(message)s'
LOGGER = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO, format=LOG_FORMAT)

kill_now = False


def exit_gracefully(signum, frame):
    LOGGER.info(f"Stopping by {signum} signal number")
    LOGGER.info(frame)
    global kill_now
    kill_now = True


def establish_connection():
    # Login
    session = requests.session()
    url = "https://stepik.org/api/xqueue/login/"
    username = os.environ["XQUEUE_USERNAME"]
    password = os.environ["XQUEUE_PASSWORD"]

    response = session.post(url, data={
        'username': username,
        'password': password,
    }, timeout=180)

    if response.status_code != 200:
        LOGGER.error("authorization error")
        return None

    return session


def ack_message(ch, delivery_tag):
    if ch.is_open:
        ch.basic_ack(delivery_tag)
    else:
        LOGGER.error("Cannot send acknowledge, channel is closed")


def reject_message(ch, delivery_tag):
    if ch.is_open:
        ch.basic_reject(delivery_tag)
    else:
        LOGGER.error("Cannot send reject, channel is closed")


def get_connection():
    connection = None
    timeout = 1
    attempt = 1
    while connection is None:
        try:
            connection = establish_connection()
        except Exception as e:
            LOGGER.error(e)
            LOGGER.error(f"Delay in {timeout} seconds after {attempt} attempt")
            sleep(timeout)
            timeout = min(300, timeout + 2)
            attempt += 1

    return connection


def rec_loads(response_string):
    response_dict = json.loads(response_string)
    for key in response_dict:
        try:
            key_dict = rec_loads(response_dict[key])
            response_dict[key] = key_dict
        except:
            pass
    return response_dict


def get_submission():
    session = get_connection()
    get_params = {'queue_name': os.environ["XQUEUE_NAME"]}
    response = session.get('https://stepik.org/api/xqueue/get_submission/', auth=None, allow_redirects=False,
                           verify=False, params=get_params)
    session.close()

    if response.status_code != 200:
        return
    response = rec_loads(response.text)
    return_code = response["return_code"]
    if return_code == 0:
        content = response["content"]
        LOGGER.info(response["content"])
        return content


def publish_task(channel, out_queue):
    content = get_submission()
    if content is None:
        return

    redis_connection = redis.Redis(host='redis', port=6379)
    redis_connection.set(content['xqueue_header']['submission_id'], json.dumps(content))

    try:
        xqueue_header = json.dumps(content.pop('xqueue_header'))
        body = json.dumps(content)

        channel.basic_publish(
            exchange='', routing_key='tasks', body=body,
            properties=pika.BasicProperties(
                reply_to=out_queue.method.queue,
                correlation_id=xqueue_header,
                delivery_mode=2
            ))

    except Exception as e:
        LOGGER.error(e)


def send_grade(ch, method, props, body, connection):
    body = json.loads(body.decode())

    LOGGER.info(f"{body}")

    redis_connection = redis.Redis(host='redis', port=6379)

    submission = redis_connection.get(json.loads(props.correlation_id)['submission_id'])
    if submission is None:
        LOGGER.error("Missing submission in redis, using the old header from the submission")
        submission = f'{{"xqueue_header": {props.correlation_id}}}'

    content = json.loads(submission)
    if 'checker_logs' in body:
        content['checker_logs'] = body['checker_logs']
    header = content['xqueue_header']

    redis_connection.set(json.loads(props.correlation_id)['submission_id'], json.dumps(content))

    threading.Thread(target=async_send_grade_part, args=(header, body, ch, method, connection)).start()


def async_send_grade_part(header, body, ch, method, connection):
    message = publish_results(header, body)

    LOGGER.info(message.json())

    ack = functools.partial(ack_message, ch, method.delivery_tag)
    reject = functools.partial(reject_message, ch, method.delivery_tag)
    if message.json()['return_code'] == 1:
        if message.json()['content'] == 'Submission is already graded':
            LOGGER.error('Submission is already graded')
            connection.add_callback_threadsafe(ack)
        else:
            LOGGER.info("Result did not reach stepik")
            connection.add_callback_threadsafe(reject)
    else:
        LOGGER.info("Result is sent")
        connection.add_callback_threadsafe(ack)


def main():
    connection = pika.BlockingConnection(pika.ConnectionParameters(host='rabbitmq'))
    channel = connection.channel()

    out_queue = channel.queue_declare('out', durable=True)
    channel.basic_qos(prefetch_count=1)  # Not to overload Stepic with requests

    on_message_callback = functools.partial(send_grade, connection=connection)
    channel.basic_consume(
        queue='out',
        on_message_callback=on_message_callback)

    LOGGER.info('Start xqueue_watcher')

    signal.signal(signal.SIGINT, exit_gracefully)
    signal.signal(signal.SIGTERM, exit_gracefully)

    while not kill_now:
        # publishing part
        sleep(1)
        publish_task(channel, out_queue)

        # receiving part
        sleep(1)
        connection.process_data_events()

    connection.close()

    LOGGER.info("exited")


def publish_results(header, body=None):
    if not body:
        body = {'score': 0, 'msg': "Try again"}

    if type(header) == dict:
        header = json.dumps(header)
    if type(body) == dict:
        score = body["score"]
        msg = body["msg"]
        body = json.dumps({"score": score, "msg": msg})

    reply = {'xqueue_body': body,
             'xqueue_header': header}

    connection = get_connection()
    result = connection.request('post', 'https://stepik.org/api/xqueue/put_result/', auth=None,
                                allow_redirects=False, data=reply, verify=False)
    connection.close()
    return result


if __name__ == '__main__':
    main()
